import numpy as np
import matplotlib.pyplot as plt

np.random.seed(476)
class Router(object):
	def __init__(self, x, y):
		self.x = x
		self.y = y
		self.neighbor = []
		self.edge=[]

class Edge(object):
	def __init__(self, x, y, l):
		self.start = x
		self.end = y
		self.len = int(int(l*10)/2+1)
		self.load = 0

class Data(object):
	def __init__(self, x, y, size, priority):
		self.now = x
		self.target = y
		self.size = size
		self.priority = priority
		self.time = 0
		self.edge = -1
		self.neigh = [priority,-1,-1,-1]

router = []
edges = []
t_edge = 0
n_router = 20

####build the graph####
for i in range(n_router):
	router.append(Router(np.random.random(),np.random.random()))

for i in range(n_router):

	dis = []
	for j in range(n_router):
		dis.append([(router[j].x - router[i].x)**2 + (router[j].y - router[i].y)**2, j])
	dis.sort(key = lambda x: x[0],reverse = False)

	for j in range(n_router):

		if len(router[i].neighbor) == 3:
			break
		if j == 0 :
			continue

		if len(router[dis[j][1]].neighbor) < 3:

			router[i].neighbor.append(dis[j][1])
			router[dis[j][1]].neighbor.append(i)

			if i<dis[j][1]:
				edges.append(Edge(i,dis[j][1],np.sqrt(dis[j][0])))
				router[i].edge.append(t_edge)
				router[dis[j][1]].edge.append(t_edge)
				t_edge += 1
			else:
				edges.append(Edge(dis[j][1],i,np.sqrt(dis[j][0])))
				router[dis[j][1]].edge.append(t_edge)
				router[i].edge.append(t_edge)
				t_edge += 1

for i in range(n_router):
	plt.scatter(router[i].x, router[i].y, color = 'orange')
for e in edges:
	plt.plot([router[e.start].x,router[e.end].x],[router[e.start].y,router[e.end].y],color='black')

data = []
n_data = 20
for i in range(n_data):
	data.append(Data(np.random.randint(n_router),np.random.randint(n_router),np.random.random(),i))

def observation(router, edges, data, n_router, n_data, t_edge):
	obs = []
	for i in range(n_data):
		ob=[]

		####meta information####
		ob.append(data[i].now)
		ob.append(data[i].target)
		ob.append(data[i].edge)
		ob.append(data[i].size)
		ob.append(data[i].priority)

		####edge information####
		for j in router[data[i].now].edge:
			ob.append(j)
			ob.append(edges[j].start)
			ob.append(edges[j].end)
			ob.append(edges[j].len)
			ob.append(edges[j].load)

		####other datas####
		count =0;
		data[i].neigh = []
		data[i].neigh.append(i)

		for j in range(n_data):
			if j==i:
				continue
			if (data[j].now in router[data[i].now].neighbor)|(data[j].now == data[i].now):
				count+=1
				ob.append(data[j].now)
				ob.append(data[j].target)
				ob.append(data[j].edge)
				ob.append(data[j].size)
				ob.append(data[i].priority)
				data[i].neigh.append(j)

			if count==3:
				break
		for j in range(3-count):
			data[i].neigh.append(-1)
			for k in range(5):
				ob.append(-1) #invalid placeholder

		obs.append(np.array(ob))

	return obs

def set_action(act,edges, data, n_data, t_edge):

	reward = [0]*n_data
	done = [False]*n_data

	for i in range(n_data):
		if data[i].edge != -1:
			data[i].time -= 1
			if data[i].time == 0:
				edges[data[i].edge].load -= data[i].size
				data[i].edge = -1

		elif act[i]==0:
			continue

		else:
			t = router[data[i].now].edge[act[i]-1]
			if edges[t].load + data[i].size >1:
				reward[i] = -0.2
			else:
				data[i].edge = t
				data[i].time = edges[t].len
				edges[t].load += data[i].size

				if edges[t].start == data[i].now:
					data[i].now = edges[t].end
				else:
					data[i].now = edges[t].start

		if data[i].now == data[i].target:
			reward[i] = 10
			done[i] = True

	return data, edges, reward, done