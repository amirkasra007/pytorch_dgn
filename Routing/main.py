import os, sys, time
import math, random, copy
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torch.autograd as autograd 
import torch.nn.functional as F
from model import DGN
from buffer import ReplayBuffer
from config import *
from routing import *

USE_CUDA = torch.cuda.is_available()
os.environ['CUDA_VISIBLE_DEVICES'] = '1'

def to_categorical(y, num_classes):
    """ 1-hot encodes a tensor """
    return np.eye(num_classes, dtype='uint8')[y]

def Adjacency(data,n_data):
	adj = []
	for j in range(n_data):
		l = to_categorical(data[j].neigh,num_classes=n_data)
		for i in range(4):
			if data[j].neigh[i] == -1:
				l[i]=np.zeros(n_data)
		adj.append(l)
	return adj

observation_space = observation(router, edges, data, n_router, n_data, t_edge)
buff = ReplayBuffer(capacity)
model = DGN(n_data,observation_space,hidden_dim,n_router)
model_tar = DGN(n_data,observation_space,hidden_dim,n_router)

model = model.cuda()
model_tar = model_tar.cuda()
optimizer = optim.Adam(model.parameters(), lr = 0.0001)

vec = np.zeros((1,neighbors))
vec[0][0] = 1

f = open('log_router_gqn.txt','w')


#######Playing#########

while(1):

	i_episode+=1	
	for i in range(n_data):
		times[i] = times[i] + 1
		if data[i].now == data[i].target:
			num+=1
			data[i].now = np.random.randint(n_router)
			data[i].target = np.random.randint(n_router)
			data[i].time = 0
			if data[i].edge != -1:
				edges[data[i].edge].load -= data[i].size
			data[i].size = np.random.rand()
			data[i].edge = -1
			total_time+=times[i]
			times[i] = 0

	obs = observation(router, edges, data, n_router, n_data, t_edge)
	adj = Adjacency(data,n_data)
	ob=[]
	for j in range(n_data):
		ob.append(np.asarray([obs[j]]))
		ob.append(np.asarray([adj[j]]))
	ob.append(np.asarray([vec]))
	action = model.predict(ob)
	act = np.zeros(n_data,dtype = np.int32)
	for j in range(n_data):
		if np.random.rand()<alpha:
			act[j]=random.randrange(action_space)
		else:
			act[j]=np.argmax(action[j])

	data, edges, reward, done = set_action(act,edges, data, n_data, t_edge)
	next_obs = observation(router, edges, data, n_router, n_data, t_edge)

	buff.add(obs, act, next_obs, reward, done, adj)

	score += sum(reward)
	if i_episode %100 ==0:
		print(int(i_episode/100))
		print(score/100,end='\t')
		f.write(str(score/100)+'\t')
		if num !=0:
			print(total_time/num,end='\t')
			f.write(str(total_time/num)+'\t')
		else :
			print(0,end='\t')
			f.write(str(0)+'\t')
		print(num,end='\t')
		print(loss/100)
		f.write(str(num)+'\t'+str(loss/100)+'\n')
		loss = 0
		score = 0
		num = 0
		total_time = 0
		
	if i_episode < episode_before_train:
		continue


	#########training#########
	batch = buff.getBatch(mini_batch)
	states,actions,rewards,new_states,dones,adj=[],[],[],[],[],[]
	for i_ in  range(n_data*2+1):
		states.append([])
		new_states.append([])
	for e in batch:
		for j in range(n_data):
			states[j*2].append(e[0][j])
			states[j*2+1].append(e[5][j])
			new_states[j*2].append(e[2][j])
			new_states[j*2+1].append(e[5][j])
		states[n_data*2].append(vec)
		new_states[n_data*2].append(vec)
		actions.append(e[1])
		rewards.append(e[3])
		dones.append(e[4])
		
	actions = np.asarray(actions)
	rewards = np.asarray(rewards)
	dones = np.asarray(dones)
		
	for i_ in  range(n_data*2+1):
		states[i_]=np.asarray(states[i_])
		new_states[i_]=np.asarray(new_states[i_])

	q_values = model.predict(states)
	target_q_values = model_tar.predict(new_states)
	for k in range(len(batch)):
		for j in range(n_data):
			if dones[k][j]:
				q_values[j][k][actions[k][j]] = rewards[k][j]
			else:
				q_values[j][k][actions[k][j]] = rewards[k][j] + GAMMA*np.max(target_q_values[j][k])

	history=model.fit(states, q_values, epochs=1, batch_size=10, verbose=0)
	his=0
	for (k,v) in history.history.items():
		his+=v[0]
	loss+=(his/n_data)
